#!/usr/bin/python
"""csefsck.py

This python script is created as part of the project for
Operating Systems(CS6233) course at NYU Tandon School of
Engineering.

A filesytem checker to check for stable condition for a
file system.

**** Please put this file inside the directory containing
	 block files
"""

__author__ = "Abhinav Jha"
__nyuid__ = "N13506352"

import os
import time

# Change to 'True' to enable debugging
DEBUG = False

SUPER_BLOCK = 0
MAX_BLOCK_SIZE = 4096
MAX_FREE_BLOCKS = 400

# Dictionary for storing corrupted blocks
error_blocks = {}

# List of blocks that are valid
valid_data_blocks = []

def check_file_inode(block):
	""" Function to check for file inode and correctness of values inside it
	Parameters:
				`block` : Block number of the file inode block
	Return:
			True		if everything is correct or corrected
			False		else
	"""

	global error_blocks

	# Add to valid blocks
	addToUsed(block)

	if DEBUG:
		print "\nChecking file inode block %s..."%block

	file_inode_dic = get_dictionary(block)

	# Special Work done for location value
	temp = file_inode_dic.get('indirect')
	temp = temp.split(' ')
	file_inode_dic['indirect'] = temp[0]
	location = temp[1].split(':')
	file_inode_dic['location'] = location[1]
	
	# Check time
	file_inode_dic['atime'] = check_time(file_inode_dic.get('atime'), block)
	file_inode_dic['ctime'] = check_time(file_inode_dic.get('ctime'), block)
	file_inode_dic['mtime'] = check_time(file_inode_dic.get('mtime'), block)

	indirect = file_inode_dic.get('indirect')
	location_block = file_inode_dic.get('location')

	addToUsed(location_block)

	size = int(file_inode_dic.get('size'))

	#Read contents pointed by the location pointer
	filename = 'FS/fusedata.%s'%location_block

	try:
		with open(filename, 'r') as f:
			data = f.read()
	except IOError:
		print "Corrupt Block %s.....Exiting" % location_block
		return False

	if indirect == '0':
		# Check to see if the file size is less than MAX_BLOCK_SIZE
		if size > MAX_BLOCK_SIZE:
			print "Block %s corrupt: Exceeds file size limit....Exiting"%block
			return False

	elif indirect == '1':

		#Check data inside block pointed by 'location'
		data_block_list = data.split(',')
		for data_block in data_block_list:
			try:
				data_block = int(data_block)
				addToUsed(data_block)
			except ValueError:
				print "Corrupt Block %s: Indirect pointer doesn't point to a list....Exiting"%block
				return False

		#Check for the size of the file 
		no_of_data_blocks = len(data_block_list)
		max_size = no_of_data_blocks*MAX_BLOCK_SIZE
		min_size = (no_of_data_blocks-1)*(MAX_BLOCK_SIZE)

		if size <= min_size or size > max_size:
			print "Corrupt Block %s: Size not in sync with data blocks assigned.....Exiting"%block
			return False
	else:
		print "Corrupt Block %s: Invalid indirect pointer......Exiting"%block
		return False

	file_inode_dic['indirect'] = file_inode_dic['indirect'] + ' location:' + file_inode_dic['location']

	if error_blocks.get(block):
		write_back_file(file_inode_dic, block)
		error_blocks.pop(block, None)

	return True

def check_directory_inode(parent, block):
	""" Function to check a directory inode block for correct values
	Parameters:
				`parent`: Block number of parent directory
				`block`: Block number of the directory block
	Return:
				True		if everything is correct or corrected
				False		else
	"""

	global DEBUG
	global error_blocks

	# Add this block to valid block list
	addToUsed(block)

	dir_dic = get_dictionary(block)

	# Check time
	dir_dic['atime'] = check_time(dir_dic.get('atime'), block)
	dir_dic['ctime'] = check_time(dir_dic.get('ctime'), block)
	dir_dic['mtime'] = check_time(dir_dic.get('mtime'), block)
	
	# Check directory size
	if int(dir_dic.get('size')) > MAX_BLOCK_SIZE:
		# Putting new size as 4096
		dir_dic['size'] = str(MAX_BLOCK_SIZE)
		error_blocks[block] = True

	# Extract filename_to_inode contents and parse information
	file_to_inode = dir_dic.get('filename_to_inode_dict')
	file_to_inode = file_to_inode[1:len(file_to_inode)-1]
	dirfile_list = file_to_inode.split(',')
	
	# Check link count
	if int(dir_dic.get('linkcount')) != len(dirfile_list):
		dir_dic['linkcount'] = len(dirfile_list)
		error_blocks[block] = True

	dot_exists = False
	dot_dot_exists = False

	# String containing corrected data for filename_to_inode key
	new_filename_to_inode = ''

	for dirfile in dirfile_list:
		try:
			dirfile = dirfile.strip()
			attributes = dirfile.split(':')
			block_type = attributes[0]
			name = attributes[1]
			child_block = attributes[2]
		except IndexError:
			print "Invalid filename_to_inode_dict.....Exiting"
			return False

		if block_type == 'd':
			# Check current working directory value
			if name == '.':
				dot_exists = True
				new_filename_to_inode = new_filename_to_inode + 'd:.:%s,'%block

				if child_block != block:
					if DEBUG:
						print "Error in current field"
					error_blocks[block] = True
			# Check parent directory value
			elif name == '..':
				dot_dot_exists = True
				new_filename_to_inode = new_filename_to_inode + 'd:..:%s,'%parent
				if child_block != parent:
					if DEBUG:
						print "Error in parent field"
					error_blocks[block] = True
			# Check child directory
			else:
				new_filename_to_inode = new_filename_to_inode + 'd:%s:%s,'%(name, child_block)
				all_ok = check_directory_inode(block, child_block)
				if not all_ok:
					return False

		# Check files inside current directory
		elif block_type == 'f':
			new_filename_to_inode = new_filename_to_inode + 'f:%s:%s,'%(name, child_block)
			all_ok = check_file_inode(child_block)
			if not all_ok:
				return False

		else:
			print "Invalid filename_to_inode_dict......Exiting"
			return False

	if not dot_exists:
		error_blocks[block] = True
		new_filename_to_inode = new_filename_to_inode + 'd:.:%s,'%block

	if not dot_dot_exists:
		error_blocks[block] = True
		new_filename_to_inode = new_filename_to_inode + 'd:..:%s,'%parent

	if error_blocks.get(block):
		new_filename_to_inode = new_filename_to_inode[0:len(new_filename_to_inode)-1]
		dir_dic['filename_to_inode_dict'] = '{' + new_filename_to_inode + '}'
		write_back_directory(dir_dic, block)
		error_blocks.pop(block, None)

	return True
		

def get_dictionary(block):
	""" Magic function that converts data inside a block into a dictionary
	Parameters:
				`block`: Block number for the block to be read
	Return:
				'dic' : Dicitonary containing data as key, value pair
	"""
	filename = 'FS/fusedata.%s'%block
	with open(filename, 'r') as f:
		data = f.read()

	dic = {}

	#Removing outer brackets
	data = data[1:len(data)-1]

	fields = data.split(',')
	for i, field in enumerate(fields):
		field = field.split(':', 1)
		key = field[0].strip()

		#Handling special case for filename_to_inode_dict field
		if key == 'filename_to_inode_dict':
			value = ','.join(fields[i+1:len(fields)])
			value = field[1].strip() + ',' + value
			dic[key] = value
			break
		else:
			value = field[1].strip()
		dic[key] = value

	return dic

def check_device_id(super_block):
	""" Function to check device ID
	Parameters:
				`super_block`: dictionary containing super block data
	Return:
			True	if device ID is correct
			False	else
	"""
	if super_block.get('devId') == '20':
		return True
	else:
		return False

def check_time(t, block):
	""" Function to check if the time in the block is before current time
	Parameters: 
				`t`: Time in UNIX EPOCH format
				`block`: Block number corresponding to the block
	Return:
			t				if time is correct
			current time	else
	"""
				
	global error_blocks
	if time.time() < float(t):
		error_blocks[block] = time.time()
		return str(time.time())
	else:
		return t

def read_free_block_list(start):
	"""Function to read a free block list in a block
	Parameters: 
				`block`: Block number of the block containing free block list
	Return:
				`free_block_list`: Free block list read from the block
	"""

	free_block_list = []

	filename = 'FS/fusedata.%s' % start
	with open(filename, 'r') as f:
		data = f.read()

	blocks = data.split(',')
	for block in blocks:
		free_block_list.append(block.strip())

	return free_block_list

def write_free_block(block, free_list):
	"""Function to write back corrected values to free block
	Parameters: 
				`block`: Block number of the block to write free block list to
				`free_list`: free_block_list to be written to the block
	"""

	print "Error in free block list......Corrected"

	filename = "FS/fusedata.%s"%block
	with open(filename, "w") as f:
		f.write(', '.join(free_list))

def check_free_blocks(max_blocks, start, end):
	""" Check free block list for 2 things:
		1. Any valid block is not listed in free block list
		2. Any block within within range (0, max_blocks-1) not 
		   imentioned in either the free block list or
		   valid block list should be written to free block list

	Parameters:
				`max_blocks`: Maximum number of blocks in the file system
				`start`: Block number of the first block containing free block list
				`end`: Block number of the last block containing free block list
	
	Return:
			True		if free block list correct or corrected
			False		else
	"""

	global valid_data_blocks

	# Keeping track of all blocks considered
	all_blocks = {}
	for i in range(max_blocks):
		if str(i) in valid_data_blocks:
			all_blocks[str(i)] = True
		else:
			all_blocks[str(i)] = False

	free_block_error = False
	free_list_sblock = {}
	
	# Loop through every free block list and check if it contains a valid block, then remove it
	for i in range(start, end+1):
		free_list_sblock[i] = read_free_block_list(i)

		# Remove valid blocks from free list if they exist
		for valid_block in valid_data_blocks:
			if valid_block in free_list_sblock:
				free_block_error = True
				free_list_sblock[i].remove(valid_block)

		for free_block in free_list_sblock[i]:
			all_blocks[free_block] = True
		
		if free_block_error:
			write_free_block(i, free_list_sblock[i])

	# Rewrite only the blocks with error or ones with some space in them

	# Blocks neither valid not listed
	add_free_blocks = []
	for block in all_blocks:
		if not all_blocks[block]:
			add_free_blocks.append(block)

	# If not listed, find a block with less than 400 blocks listed to write it
	not_listed_blocks = len(add_free_blocks)
	if not_listed_blocks:
		for i in range(start, end+1):
			available_slots = MAX_FREE_BLOCKS - len(free_list_sblock[i])
			if not available_slots:
				continue
			if available_slots > not_listed_blocks:
				for block in add_free_blocks:
					free_list_sblock[i].append(block)
				
				write_free_block(i, free_list_sblock[i])
				return True
			else:
				for j in range(available_slots):
					free_list_sblock[i].append(add_free_blocks[j])
					add_free_blocks.pop(j)
					not_listed_blocks = not_listed_blocks - 1
					if not_listed_blocks == 0:
						return True
				write_free_blocks(i, free_list_sblock[i]) 

	return True

def addToUsed(block):
	""" Function to append block as valid block in valid_data_blocks list
		Parameters:	
					`block`: Valid block
	"""

	# Append all blocks to valid block list
	global valid_data_blocks
	valid_data_blocks.append(block)

def write_back_super(super_block):
	"""Function to write back corrected values to super block
	Parameters: 
				`super_block`: dictionary containing corrected data to be written
	"""

	print "Error in super block.....Corrected"

	#Writing in order
	contents = '{creationTime:' + super_block.get('creationTime') +\
				', mounted:' + super_block.get('mounted') +\
				', devId:' + superblock.get('devId') +\
				', freeStart:' + superblock.get('freeStart') +\
				', freeEnd:' + superblock.get('freeEnd') +\
				', root:' + superblock.get('root') +\
				', maxBlocks:' + superblock.get('maxBlocks') + '}'

	filename = 'FS/fusedata.%s'%SUPER_BLOCK
	with open(filename,"w") as f:
		f.write(contents)

def write_back_directory(dir_dic, block):
	"""Function to write back corrected values to directory inode
	Parameters: 
				`dir_dic`: dictionary containing corrected data to be written
				`block`: corrupted block number
	"""

	print "Error in directory inode block %s.....Corrected"%block

	if DEBUG:
		print "Dictionary to be written:  %s"%(str(dir_dic))

	#Writing in order
	contents = '{size:' + dir_dic.get('size') +\
				', uid:' + dir_dic.get('uid') +\
				', gid:' + dir_dic.get('gid') +\
				', mode:' + dir_dic.get('mode') +\
				', atime:' + dir_dic.get('atime') +\
				', ctime:' + dir_dic.get('ctime') +\
				', mtime:' + dir_dic.get('mtime') +\
				', linkcount:' + dir_dic.get('linkcount') +\
				', filename_to_inode_dict:' + dir_dic.get('filename_to_inode_dict') + '}'

	filename = 'FS/fusedata.%s'%block
	with open(filename,"w") as f:
		f.write(contents)

def write_back_file(file_inode_dic, block):
	"""Function to write back corrected values to file inode
	Parameters: 
				`file_inode_dic`: dictionary containing corrected data to be written
				`block`: corrupted block number
	"""

	print "Error in file inode block %s.....Corrected"%block

	if DEBUG:
		print "Dictionary to be written:  %s"%(str(file_inode_dic))

	#Writing in order
	contents = '{size:' + file_inode_dic.get('size') +\
				', uid:' + file_inode_dic.get('uid') +\
				', gid:' + file_inode_dic.get('gid') +\
				', mode:' + file_inode_dic.get('mode') +\
				', linkcount:' + file_inode_dic.get('linkcount') +\
				', atime:' + file_inode_dic.get('atime') +\
				', ctime:' + file_inode_dic.get('ctime') +\
				', mtime:' + file_inode_dic.get('mtime') +\
				', indirect:' + file_inode_dic.get('indirect') + '}'

	filename = 'FS/fusedata.%s'%block
	with open(filename,"w") as f:
		f.write(contents)

def check_super_block():
	"""Check super block for valid data.
		
	Return: 
			`root_block`: root block number
			`max_blocks`: total number of blocks in the file system
			`freeStart`: block number where list of free blocks starts
			`freeEnd`: block number where list of free blocks ends
	"""
	#Get super block contents
	super_block = get_dictionary(SUPER_BLOCK)
	
	# Append super block to valid_block_list
	addToUsed(str(SUPER_BLOCK))
	
	if DEBUG:
		print "Super block dictionary: \n%s"%super_block

	root_block = super_block.get('root')
	max_blocks = int(super_block.get('maxBlocks'))
	free_blocks_start = int(super_block.get('freeStart'))
	free_blocks_end = int(super_block.get('freeEnd'))

	if not root_block or not max_blocks or not free_blocks_start or not free_blocks_end:
		print "Super block corrupt....Exiting"
		return None

	
	if not check_device_id(super_block):
		super_block['devID'] = '20'
		error_blocks[SUPER_BLOCK] = True

	# Check time for creation block
	super_block['creationTime'] = check_time(super_block.get('creationTime'), SUPER_BLOCK)

	# If error in super_block, write the corrected contents back
	if error_blocks.get(SUPER_BLOCK):
		write_back_super(super_block)
		error_blocks.pop(SUPER_BLOCK, None)

	return root_block, max_blocks, free_blocks_start, free_blocks_end
	

def run_fsck():
	"""Main function to run file system checker"""
	
	print "Running file system checker...."

	super_block_data = check_super_block()
	if not super_block_data:
		print "File System Corrupt"
		return
	root_block, max_blocks, free_blocks_start, free_blocks_end = super_block_data

	# Start from root block
	all_ok = check_directory_inode(root_block, root_block)
	if not all_ok:
		print "File System Corrupt"
		return

	# Check for free blocks

	# Append all blocks containing free block list to valid block list
	i = free_blocks_start
	while(i<=free_blocks_end):
		addToUsed(str(i))
		i = i + 1

	all_ok = check_free_blocks(max_blocks, free_blocks_start, free_blocks_end)

	if len(error_blocks) or not all_ok:
		print "File System Corrupt"
	else:
		print "File System OK"


if __name__ == "__main__":
	run_fsck()
